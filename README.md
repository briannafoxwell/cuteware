# Cuteware
cute software for cute girls (or whoever likes cute stuff)

- [catgirl](https://git.causal.agency/catgirl/about/) - cute irc client for girls of the nya variety
- [cgit-pink](https://git.causal.agency/cgit-pink/about/) - cute  cgit fork
- [drawterm](https://github.com/9fans/drawterm) - connect to plan9 from any OS!
- [kirc](https://github.com/mcpcpc/kirc) - tiny irc client, very simple and cute
- [cmus](https://github.com/cmus/cmus) - command line music player for soft girls who hate graphical applications
- [powertoys](https://github.com/microsoft/PowerToys) - power user tools for everyone (and cute girls!)
- [kakoune](https://github.com/mawww/kakoune) - basically vim but for nerdy cute girls 